import axios from "axios"
import store from '../store'
import router from '../router'
// axios.defaults.baseURL="http://localhost:8888/api/private/v1/"
// axios.defaults.headers['Authorization']=localStorage.getItem('token')||''
const instance = axios.create({
    baseURL:"http://localhost:8888/api/private/v1/",
    // headers['Authorization']:localStorage.getItem('token')||''
})

//请求拦截器
instance.interceptors.request.use((config) => {
    if (store.state.token) {
        config.headers['Authorization'] = store.state.token
    }
    return config
    // else {
    //     router.push({
    //         name:"login"
    //     })
    // }
    //  路由跳转冲突: 刚进入登录页面时候 token没有,  也会走这个else 登录页面跳转登录页面  所以冲突
},(error) =>{
    return Promise.reject(error)
})

//响应拦截器
instance.interceptors.response.use((response)=>{
    if (response.data.meta.status=="无效token") {
        router.push({
            name:"login"
        })
    }
    return response
},(error) => {
    return Promise.reject(error)
})
export function http(url,method,data,params){
    return new Promise((resolve,reject) =>{
        instance({
            url,
            method,
            data,
            params
        }).then(res => {
            console.log(res)
            if(res.status >= 200 && res.status < 300 || res.status === 304) {
                if(res.data.meta.status === 200) {
                    // Message({
                    //     showClose: true,
                    //     message: '登录成功',
                    //     type:"success"
                    //   })
                    resolve(res.data.data)
                }else {
                    reject(res.data.meta)
                }
            }else {
                reject(res)
            }
        }).catch(err => {
            reject(err)
        })
    })
}