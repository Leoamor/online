import {http} from './'

// 登录
export function login (data){
    return http("login","post",data)
}

// 获取左侧权限列表
export function getMenus (){
    return http("menus","GET")
}

// 获取用户列表
export function getUserList(params){
    return http("users","GET","",params)
}

// 获取角色列表
export function getRoleList (params){
    return http("users","GET","",params)
}
// getRoleList(
//     {
//       pagenum: 1,
//       pagesize: 10
//     }
//   ).then(res => {
//     Message({
//       showClose: true,
//       message: '登录成功',
//       type:"success"
//     })
//     console.log(res);
//   });