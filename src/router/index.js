import Vue from 'vue'
import VueRouter from 'vue-router'
Vue.use(VueRouter)

const routes = [
  {path:"/",redirect:"/login"},
  {
    path: '/login',
    name: 'login',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "login" */ '../components/login.vue')
  },
  {
    path: '/index',
    // name: 'index',
    component: () => import(/* webpackChunkName: "index" */ '../components/Index.vue'),
    children:[
      {path:"/",redirect:"users",name:"index"},
      {
        path:"users",
        name:"users",
        component: () => import(/* webpackChunkName: "users" */ '../components/users/users.vue'),
      },
      {
        path:"rights",
        name:"rights",
        component: () => import(/* webpackChunkName: "rights" */ '../components/rights/rights.vue'),
      },
      {
        path:"roles",
        name:"roles",
        component: () => import(/* webpackChunkName: "roles" */ '../components/rights/roles.vue'),
      },
      {
        path:"orders",
        name:"orders",
        component: () => import(/* webpackChunkName: "orders" */ '../components/orders/orders.vue'),
      },
      {
        path:"reports",
        name:"reports",
        component: () => import(/* webpackChunkName: "reports" */ '../components/reports/reports.vue'),
      },
      {
        path:"goods",
        name:"goods",
        component: () => import(/* webpackChunkName: "goods" */ '../components/goods/goods.vue'),
      },
      {
        path:"params",
        name:"params",
        component: () => import(/* webpackChunkName: "params" */ '../components/goods/params.vue'),
      },
      {
        path:"categories",
        name:"categories",
        component: () => import(/* webpackChunkName: "categories" */ '../components/goods/categories.vue'),
      }
    ]
  }
]



const router = new VueRouter({
  mode: 'hash',
  base: process.env.BASE_URL,
  routes
})
router.beforeEach((to, from, next) => {
  next()
})
export default router
