import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

import "./lib/reset.css"

import "./font/iconfont.css"
import "./font/iconfont.svg"


import ElementUI from 'element-ui'
import "element-ui/lib/theme-chalk/index.css"
Vue.use(ElementUI, { size: 'small', zIndex: 3000 });
// 项目中所有拥有 size 属性的组件的默认尺寸均为 'small'，弹框的初始 z-index 为 3000。
Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
