import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

function initialState(){
  return {
    token:localStorage.getItem("token")||"",
    username:localStorage.getItem("username")||""
  }
}

export default new Vuex.Store({
  state: initialState(),
  mutations: {
    setToken(state, data){
      state.token = data;
      localStorage.setItem("token",state.token)
    },
    setUsername(state, data){
      state.username = data;
      localStorage.setItem("username",state.username)
    },
    resetState(state){
      Object.assign(state,initialState())
    }
  },
  actions: {
  },
  modules: {
  }
})
